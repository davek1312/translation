# davek1312/translation

Bootstraps illuminate/translation package.

# Installation

The package is available on [Packagist](https://packagist.org/packages/davek1312/translation),
you can install it using [Composer](https://getcomposer.org/).

```bash
composer require davek1312/translation
```

# Configuration

Copy the `vendor/davek1312/translation/davek1312` folder to your application's root directory.

To define a default locale edit  `/davek1312/translation/config/translation.php`:
```php
<?php

return [

    'default_locale' => 'en',

];
```

# Language Files

## Application Language Files
Language files are stored in `davek1312/translation/lang`. For each language your application supports you should have a subdirectory:

    /davek1312
        /translation
            /lang
                /en
                test.php
                /fr
                test.php

Each language file is just a simple associative array e.g:
```php
<?php 

return [
    'key' => 'value',
];
```

## Package Language Files
If you are developing a package and would like to use language files you can create them in the same was as above but you also need to register the language files.
To register your language files view the [davek1312\app](https://packagist.org/packages/davek1312/app) documentation.

# Usage

## Basic usage
```php
<?php 

use Davek1312\Translation\Translator;

$translator = new Translator();

//Retrieve a value language value for a key using the default locale
$translator->trans('file-name.key');

//Retrieve a value language value for a key with a different locale 
$translator->trans('file-name.key', [], 'fr');

//Retrieve a packages langauge file value
$translator->trans('package-name::file-name.key');
```

## Replacing Parameters In Language String
You can create parameters in your language file by prefixing text with `:`
```php
'welcome' => 'Hello :name'
```

To replace the parameters with values supply an array of parameter values:
```php
// This will return "Hello David"
$translator->trans('file-name.welcome', ['name' => 'David']);
```

## Overriding Package Language Files
If you want to override a packages you can place the overriding files in `davek1312/translation/lang/vendor/{package-name}/{locale}`.

For example if you want to override values in `messages.php` in a package called `other-package` you would create the following file: 
`davek1312/translation/lang/vendor/{other-package}/{locale}/messages.php`.
Within this file, you should only define the translation strings you wish to override. Any translation strings you don't override will still be loaded from the package's original language files.