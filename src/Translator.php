<?php

namespace Davek1312\Translation;

use Davek1312\App\App;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Translation\FileLoader;

/**
 * Wrapper formIlluminate\Translation\Translator
 *
 * @author  David Kelly <davek1312@gmail.com>
 */
class Translator extends \Illuminate\Translation\Translator {

    /**
     * @var App
     */
    private $app;

    /**
     * Translator constructor.
     *
     * @param App|null $app
     */
    public function __construct(App $app = null) {
        parent::__construct($this->getFileLoader(), $this->getDefaultLocale());
        $this->app = $app ? $app : new App();
        $this->registerPackageLangFiles();
    }

    /**
     * @return FileLoader
     */
    private function getFileLoader() {
        return new FileLoader(new Filesystem(), Registry::getRootPackageLangDir());
    }

    /**
     * @return string
     */
    private function getDefaultLocale() {
        $registry = new Registry();
        return $registry->getDefaultLocale();
    }

    /**
     * Registers the pacakges language files as namespaces translations
     *
     * @return void
     */
    private function registerPackageLangFiles() {
        $langFiles = $this->app->getLang();
        foreach($langFiles as $namespace => $pathToLang) {
            $this->addNamespace($namespace, $pathToLang);
        }
    }

    /**
     * @return App
     */
    public function getApp() {
        return $this->app;
    }

    /**
     * @param App $app
     */
    public function setApp($app) {
        $this->app = $app;
    }
}