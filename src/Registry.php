<?php

namespace Davek1312\Translation;

use Davek1312\App\App;
use Davek1312\Config\Config;

/**
 * Registers davek1312 components
 *
 * @author  David Kelly <davek1312@gmail.com>
 */
class Registry extends \Davek1312\App\Registry {

    /**
     * @var Config
     */
    private $configInstance;

    /**
     * Register the database configuration file
     *
     * @return void
     */
    protected function register() {
        $this->registerConfig('davek1312/translation/config');
    }

    /**
     * @return string
     */
    public function getDefaultLocale() {
        return $this->getFromConfig('default_locale');
    }

    /**
     * @param $key
     *
     * @return mixed
     */
    private function getFromConfig($key) {
        if(!$this->configInstance) {
            $this->configInstance = new Config();
        }
        return $this->configInstance->get("translation.$key");
    }

    /**
     * Return the location where the root package language files are located
     *
     * @return string
     */
    public static function getRootPackageLangDir() {
        $app = new App();
        return "{$app->getRootPackageDir()}davek1312/translation/lang";
    }
}