<?php

namespace Davek1312\Translation\Tests;

use Davek1312\App\App;
use Davek1312\Translation\Translator;

class TranslatorTest extends \PHPUnit_Framework_TestCase {

    /**
     * @var Translator
     */
    private $translator;
    /**
     * @var App
     */
    private $app;

    public function setUp() {
        parent::setUp();
        $this->app = new App();
        $lang = $this->app->getLang();
        $lang['mock-package'] = __DIR__.'/Mock/lang';
        $this->app->setLang($lang);
        $this->translator = new Translator($this->app);
    }

    public function test__construct() {
        $this->assertEquals('en', $this->translator->getLocale());
        $this->assertEquals([
            'test' => 'value',
        ], $this->translator->getLoader()->load('en', 'test'));
        $this->assertEquals($this->app, $this->translator->getApp());
        $this->assertRegisterPackageLangFiles();
    }

    private function assertRegisterPackageLangFiles() {
        $this->assertEquals([
            'mock-package' => __DIR__.'/Mock/lang',
        ], $this->translator->getLoader()->namespaces());
        $this->assertEquals([
            'mock-test' => 'mock-value',
        ], $this->translator->trans('mock-package::test'));
    }
}