<?php

namespace Davek1312\Translation\Tests;

use Davek1312\Translation\Registry;

class RegistryTest extends \PHPUnit_Framework_TestCase {

    private $registry;

    public function setUp() {
        parent::setUp();
        $this->registry = new Registry();
    }

    public function testConfigRegistered() {
        $this->assertEquals([
            'davek1312/translation/config',
        ], $this->registry->getConfig());
    }

    public function testGetDefaultLocale() {
        $this->assertEquals('en', $this->registry->getDefaultLocale());
    }

    public function testGetRootPackageLangDir() {
        $this->assertContains('davek1312/../davek1312/translation/lang', Registry::getRootPackageLangDir());
    }
}